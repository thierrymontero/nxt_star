# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError,ValidationError
import openpyxl
from openpyxl import Workbook
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font, colors
from openpyxl.styles.borders import Border, Side
from openpyxl.drawing.image import Image
from openpyxl.utils import get_column_letter
from openpyxl.styles import Alignment, Font
from openpyxl.utils import FORMULAE
import tempfile
import shutil
from openpyxl.worksheet.dimensions import Dimension
from openpyxl.cell import *
import datetime
from datetime import datetime,timedelta
import base64
import xlwt
import xlsxwriter
import time
from dateutil.rrule import rrule, MONTHLY
from dateutil.relativedelta import relativedelta
import calendar

class ReportAverage(models.TransientModel):
    _name = 'star.step.average'

    job_id = fields.Many2one(
        string="Job concerné",
        comodel_name="hr.job",
    )
    department_id = fields.Many2one(
        string="Département",
        comodel_name="hr.department",
    )
    
    responsable = fields.Many2one(
        string="Responsable",
        comodel_name="res.users",
        ondelete="set null",
    )
    applicant_id = fields.Many2one(comodel_name="hr.applicant", string='Candidat')
    date_from=fields.Date(string="Date debut",default=fields.Date.today)
    date_to=fields.Date(string="Date fin",onchange='check_date(date_to)')
    is_active = fields.Boolean(string="Inclure les candidats inactifs",default=False)
    moyenne_name=fields.Char()
    moyenne_file=fields.Binary(readonly=True)
    
    obj_step_one = fields.Integer(
        string='Objectif Step1',default=3)
    obj_step_two = fields.Integer(
        string='Objectif Step2',default=7
    )
    obj_step_free = fields.Integer(
        string='Objectif Step3',default=7
    )
    obj_step_four = fields.Integer(
        string='Objectif Step4',default=3
    )
    obj_step_five = fields.Integer(
        string='Objectif Step5',default=3
    )
    obj_step_six = fields.Integer(
        string='Objectif Step6',default=8
    )
    obj_step_seven = fields.Integer(
        string='Objectif clôture',default=31
    )
    takt = fields.Integer(
        string='DEP Cible',default=31
    )
    lss = fields.Integer(
        string='LSS',default=38
    )
    
    @api.onchange('applicant_id','is_applicant_only')
    def onchange_applicant_id_somapr(self):
        if self.applicant_id:
            self.is_applicant_only = True
            self.department_id = self.job_id = self.responsable = self.date_to = self.date_from = None
           
    is_applicant_only = fields.Boolean(string="Un seul candidat",invisible=1)
    

    @api.multi
    @api.constrains('date_from','date_to')
    def check_date(self):
        for date in self:
            if date.date_from and date.date_to:
                if date.date_from > date.date_to:
                    raise ValidationError("La date de fin doit être supérieure à la date de debut.")
        
    @api.onchange('job_id')
    def onchange_job_id(self):
        if self.job_id:
            vals = self.env['hr.job'].browse(self.job_id.id)
            vals = vals[0].mapped('department_id')
            self.department_id = vals
    
    def get_date(self,applicant_id,step):
        obj_stage_history = self.env['stage.history']
        stage = obj_stage_history.sudo().search([('applicant_id','=',applicant_id),('step','=',step)],order="id DESC",limit=1)
        if stage:
            date = datetime.strptime(stage.date,'%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y')
            return date

    def get_date_last_step(self,applicant_id):
        step = [12,3,2]
        obj_stage_history = self.env['stage.history']
        stage_last = obj_stage_history.sudo().search([('applicant_id','=',applicant_id),('step','in',step)],order="id DESC",limit=1)
        if stage_last:
            date_last = datetime.strptime(stage_last.date,'%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y')
            return date_last
    
    def check_value(self,value):
        if value:
            return value
        else:
            return 0

    @api.multi
    def export_moyenne(self):
        tmpdir = tempfile.mkdtemp()
        tmpdir = tmpdir.rstrip('/')
                
        wb = Workbook()
        ws = wb.active
        # sheet = wb.worksheets[0]
        ws.page_setup.orientation = ws.ORIENTATION_LANDSCAPE
        ws.page_setup.paperSize = ws.PAPERSIZE_TABLOID
        ws.page_setup.fitToHeight = 0
        ws.page_setup.fitToWidth = 1
        row = ws.row_dimensions[1]
        center = Alignment(horizontal='center',
                                   vertical='center')
        ft = Font(name="Calibri", size=10)
        ft_entete = Font(name="Calibri", size=10, color='00000000', bold=True)
        ft_white = Font(name="Calibri", size=10, color='FFFFFFFF', bold=False)
        ft_white_bold = Font(name="Calibri", size=10, color='FFFFFFFF', bold=True)
     
        yellow = PatternFill(start_color='FFFFFF00',
                   end_color='FFFFFF00',fill_type='solid')
       
        border = Border(top=Side(border_style='thin',color='FF000000'),bottom=Side(border_style='thin',color='FF000000'),right=Side(border_style='thin',color='FF000000'),left=Side(border_style='thin',color='FF000000'))
        border_white = Border(top=Side(border_style='thin',color='FFFFFFFF'),bottom=Side(border_style='thin',color='FFFFFFFF'),right=Side(border_style='thin',color='FFFFFFFF'),left=Side(border_style='thin',color='FFFFFFFF'))
       
        domain = [('create_date','>=',self.date_from),('create_date','<=',self.date_to)]
        
        if self.job_id:
            domain.append(('job_id','=',self.job_id.id))
        if self.department_id:
            domain.append(('department_id','=',self.department_id.id))
        if self.responsable:
            domain.append(('user_id','=',self.responsable.id))
        if not self.is_active:
            domain.append(('active','=',True))
            
        
        ##### AJOUTER UNE PERSONNE SEULEMENT ####
        all_applicant = self.env['hr.applicant'].sudo().search(domain)
        if self.applicant_id:
            all_applicant = self.env['hr.applicant'].sudo().browse(self.applicant_id.id)
        
       
        date_from = ''
        date_to = ''
        if self.date_from and self.date_to:
            date_from = datetime.strptime(self.date_from, '%Y-%m-%d')
            date_to = datetime.strptime(self.date_to, '%Y-%m-%d')
            date_from = datetime.strftime(date_from, '%d/%m/%Y')
            date_to = datetime.strftime(date_to, '%d/%m/%Y')
       
        l=1
        ws.row_dimensions[l].height = 28
        ws['A%s' % l] = 'Rapport du %s au %s'%(date_from,date_to)
        ws['A%s' % l].font = ft_entete
        l = 2
        ws.row_dimensions[l].height = 28
        ws['B%s' % l] = "RESPONSABLE: %s"%(self.responsable.name)
        ws['B%s' % l].font = ft_entete
        ws['C%s' % l] = "DEPARTEMENT: %s"%(self.department_id.name)
        ws['D%s' % l] = "JOB CONCERNE: %s"%(self.job_id.name)
        if not self.responsable:
            ws['B%s' % l] = "RESPONSABLE: Tous"
        if not self.department_id:
            ws['C%s' % l] = "DEPARTEMENT: Tous"
        if not self.job_id:
            ws['D%s' % l] = "JOB CONCERNE: *"
        ws['J%s'% l]= "Réception => Check historique"
        ws['K%s'% l]= "Check historique => Test"
        ws['L%s'% l]= "Test => ENT RH"
        ws['M%s'% l]= "ENT RH => EM"
        ws['N%s'% l]= "EM => ENT N+1"
        ws['O%s'% l]= "ENT N+1 => Clôture"
        
        l = 3
        ws.row_dimensions[l].height = 28
        ws['B%s' % l] = "Nom et prénoms"
        ws['B%s' % l].border = border
        ws['B%s' % l].alignment = center
        ws['C%s' % l] = "Date de réception"
        ws['C%s' % l].border = border
        ws['C%s' % l].alignment = center
        ws['D%s' % l] = "Date check historique"
        ws['D%s' % l].border = border
        ws['D%s' % l].alignment = center
        ws['E%s' % l] = "Date test"
        ws['E%s' % l].border = border
        ws['E%s' % l].alignment = center

        ws['F%s' % l] = "Date ENT RH"
        ws['F%s' % l].border = border
        ws['F%s' % l].alignment = center
        ws['G%s' % l] = "Date EM"
        ws['G%s' % l].border = border
        ws['G%s' % l].alignment = center
        ws['H%s' % l] = "Date ENT N+1"
        ws['H%s' % l].border = border
        ws['H%s' % l].alignment = center
        ws['I%s' % l] = "Date clôture (retenue, non retenue ou vivier)"
        ws['I%s' % l].border = border
        ws['I%s' % l].alignment = center
        ws['J%s' % l] = "Step 1"
        ws['J%s' % l].border = border
        ws['J%s' % l].alignment = center
        ws['J%s' % l].font = ft_white_bold
        ws['K%s' % l] = "Step 2"
        ws['K%s' % l].border = border
        ws['K%s' % l].alignment = center
        ws['K%s' % l].font = ft_white_bold
        ws['L%s' % l] = "Step 3"
        ws['L%s' % l].border = border
        ws['L%s' % l].alignment = center
        ws['L%s' % l].font = ft_white_bold
        ws['M%s' % l] = "Step 4"
        ws['M%s' % l].border = border
        ws['M%s' % l].alignment = center
        ws['M%s' % l].font = ft_white_bold
        ws['N%s' % l] = "Step 5"
        ws['N%s' % l].border = border
        ws['N%s' % l].alignment = center
        ws['N%s' % l].font = ft_white_bold
        ws['O%s' % l] = "Step 6"
        ws['O%s' % l].border = border
        ws['O%s' % l].alignment = center
        ws['O%s' % l].font = ft_white_bold
        ws['P%s' % l] = "clôture/j calendaires"
        ws['P%s' % l].border = border
        ws['P%s' % l].alignment = center
        ws['P%s' % l].font = ft_white_bold
        ws['Q%s' % l] = "contrôle"
        ws['Q%s' % l].border = border
        ws['Q%s' % l].alignment = center
        ws['R%s' % l] = ""
        ws['R%s' % l].border = border
        ws['R%s' % l].alignment = center
        ws['S%s' % l] = "Moyenne"
        ws['S%s' % l].border = border
        ws['S%s' % l].alignment = center
        ws['S%s' % l].font = ft_entete
        
        ws['T%s' % l] = "Ecart-type"
        ws['T%s' % l].border = border
        ws['U%s' % l] = "LCS"
        ws['U%s' % l].border = border
        ws['V%s' % l] = "LCI"
        ws['V%s' % l].border = border
        ws['W%s' % l] = "Objectif"
        ws['W%s' % l].border = border
        ws['X%s' % l] = "LSS"
        ws['X%s' % l].border = border
       
        
        for app in all_applicant:
            l+=1
            ws['B%s' % l] = app.partner_name
            ws['B%s' % l].border = border
            ws['B%s' % l].alignment = center
            ws['C%s' % l] = datetime.strptime(app.create_date,'%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y')
            ws['C%s' % l].border = border
            ws['C%s' % l].alignment = center
            #get_date "PRESELECTION - CHECK HISTORIQUE"
            ws['D%s' % l] = self.get_date(app.id,18)
            ws['D%s' % l].border = border
            ws['D%s' % l].alignment = center
            #get_date_"TESTS DE RECRUTEMENT"
            ws['E%s' % l] = self.get_date(app.id,4)
            ws['E%s' % l].border = border
            ws['E%s' % l].alignment = center
            #get_date"ENTRETIEN RH"
            ws['F%s' % l] = self.get_date(app.id,17)
            ws['F%s' % l].border = border
            ws['F%s' % l].alignment = center
            #get_date"ENQUETE DE MORALITE"
            ws['G%s' % l] = self.get_date(app.id,13)
            ws['G%s' % l].border = border
            ws['G%s' % l].alignment = center
            #get_date"ENTRETIEN N+1"
            ws['H%s' % l] = self.get_date(app.id,14)
            ws['H%s' % l].border = border
            ws['H%s' % l].alignment = center
            #get_date "retenue, non retenue ou vivier"
            ws['I%s' % l] = self.get_date_last_step(app.id)
            ws['I%s' % l].border = border
            ws['I%s' % l].alignment = center

            #Reception => Check_historique
            ws['J%s' % l] = '=_xlfn.IF(OR(D%s="",C%s=""),"",D%s-C%s)'%(l,l,l,l)
            ws['J%s' % l].alignment = center
            ws['J%s' % l].font = ft_white
                
            #Check_historique => Test
            # '=_xlfn.IF(D%s="";"";D%s-C%s'%(l,l,l)
            ws['K%s' % l] = '=_xlfn.IF(OR(E%s="",D%s=""),"",E%s-D%s)'%(l,l,l,l)
            # ws['K%s' % l] = '=E%s'%l+'-'+'D%s'%l
            ws['K%s' % l].alignment = center
            ws['K%s' % l].font = ft_white
                
            #Test => ENT RH
            ws['L%s' % l] = '=_xlfn.IF(OR(F%s="",E%s=""),"",F%s-E%s)'%(l,l,l,l)
            # ws['L%s' % l] = '=F%s'%l+'-'+'E%s'%l
            ws['L%s' % l].alignment = center
            ws['L%s' % l].font = ft_white
                
            #ENT RH => EM
            ws['M%s' % l] = '=_xlfn.IF(OR(G%s="",F%s=""),"",G%s-F%s)'%(l,l,l,l)
            ws['M%s' % l].alignment = center
            ws['M%s' % l].font = ft_white
          
            # EM => ENT N+1
            ws['N%s' % l] = '=_xlfn.IF(H%s="","",IF(G%s="",IF(OR(F%s="",H%s=""),"",H%s-F%s),H%s-G%s))'%(l,l,l,l,l,l,l,l)
            ws['N%s' % l].alignment = center
            ws['N%s' % l].font = ft_white
               
                # step_five.append(ws['N%s' % l].value)
          
            # ENT N+1 => Clôture
            ws['O%s' % l] = '=_xlfn.IF(H%s="",IF(OR(F%s="",I%s=""),"",I%s-F%s),IF(I%s="","",I%s-H%s))'%(l,l,l,l,l,l,l,l)
            ws['O%s' % l].alignment = center
            ws['O%s' % l].font = ft_white
                
            #Cloture
            
            # ws['P%s' % l] = '=+I%s-C%s'%(l,l)
            ws['P%s' % l] = '=_xlfn.IF(I%s="","",I%s-C%s)'%(l,l,l)
            ws['P%s' % l].alignment = center
            ws['P%s' % l].font = ft_white
            
            #Contrôle
            ws['Q%s' % l] = '=_xlfn.SUM(J%s:O%s)'%(l,l)
            ws['Q%s' % l].alignment = center
            
            #difference
            # ws['R%s' % l] = '=+P%s'%l+'-'+'Q%s'%l
            ws['R%s' % l] =  '=_xlfn.IF(P%s="","",P%s-Q%s)'%(l,l,l)
            ws['R%s' % l].alignment = center

            ws['S%s' % l] = "Moyenne"
            ws['T%s' % l] = "Ecart-type"
            ws['U%s' % l] = "LCS"
            ws['V%s' % l] = "LCI"
            ws['W%s' % l] = "Objectif"
            ws['X%s' % l] = "LSS"
        l+=1
       
        #####
        ws['I%s' % l] = 'MOYENNE'
        ws['I%s' % l].font = ft_entete
        ws['I%s' % l].border = border
        
        ws['J%s' % l] = '=_xlfn.AVERAGE(J4:J%s)'% str(l-1)
        ws['J%s' % l].border = border
        ws['K%s' % l] = '=_xlfn.AVERAGE(K4:K%s)'% str(l-1)
        ws['K%s' % l].border = border
        ws['L%s' % l] = '=_xlfn.AVERAGE(L4:L%s)'% str(l-1)
        ws['L%s' % l].border = border
        ws['M%s' % l] = '=_xlfn.AVERAGE(M4:M%s)'% str(l-1)
        ws['M%s' % l].border = border
        ws['N%s' % l] = '=_xlfn.AVERAGE(N4:N%s)'% str(l-1)
        ws['N%s' % l].border = border
        ws['O%s' % l] = '=_xlfn.AVERAGE(O4:O%s)'% str(l-1)
        ws['O%s' % l].border = border
        ws['P%s' % l] = '=_xlfn.AVERAGE(P4:P%s)'% str(l-1)
        ws['P%s' % l].border = border
        ws['P%s' % l].fill = yellow
        l+=1

        ws['I%s' % l] = 'ECART-TYPE'
        ws['I%s' % l].border = border
        ws['I%s' % l].font = ft_entete
        ws['J%s' % l] = '=_xlfn.STDEV.S(J4:J%s)'% str(l-2)
        ws['J%s' % l].border = border
        ws['K%s' % l] = '=_xlfn.STDEV.S(K4:K%s)'% str(l-2)
        ws['K%s' % l].border = border
        ws['L%s' % l] = '=_xlfn.STDEV.S(L4:L%s)'% str(l-2)
        ws['L%s' % l].border = border
        ws['M%s' % l] = '=_xlfn.STDEV.S(M4:M%s)'% str(l-2)
        ws['M%s' % l].border = border
        ws['N%s' % l] = '=_xlfn.STDEV.S(N4:N%s)'% str(l-2)
        ws['N%s' % l].border = border
        ws['O%s' % l] = '=_xlfn.STDEV.S(O4:O%s)'% str(l-2)
        ws['O%s' % l].border = border
        ws['P%s' % l] = '=_xlfn.STDEV.S(P4:P%s)'% str(l-2)
        ws['P%s' % l].border = border
        ws['P%s' % l].fill = yellow        
        
        # ws['I%s' % l] = 'ECART-TYPE'
        # ws['I%s' % l].font = ft_entete
        # ws['J%s' % l] = round(np.std(self.check_value(step_one)))
        # ws['K%s' % l] = round(np.std(self.check_value(step_two)))
        # ws['L%s' % l] = round(np.std(self.check_value(step_free)))
        # ws['M%s' % l] = round(np.std(self.check_value(step_four)))
        # ws['N%s' % l] = round(np.std(self.check_value(step_five)))
        # ws['O%s' % l] = round(np.std(self.check_value(step_six)))
        # ws['P%s' % l] = round(np.std(self.check_value(cloture)))
        l+=1
        ws['I%s' % l] = 'OBJECTIF'
        ws['I%s' % l].border = border
        ws['I%s' % l].font = ft_entete
        ws['J%s' % l] = self.obj_step_one
        ws['J%s' % l].border = border
        ws['K%s' % l] = self.obj_step_two
        ws['K%s' % l].border = border
        ws['L%s' % l] = self.obj_step_free
        ws['L%s' % l].border = border
        ws['M%s' % l] = self.obj_step_four
        ws['M%s' % l].border = border
        ws['N%s' % l] = self.obj_step_five
        ws['N%s' % l].border = border
        ws['O%s' % l] = self.obj_step_six
        ws['O%s' % l].border = border
        ws['P%s' % l] = self.obj_step_seven
        ws['P%s' % l].border = border
        ws['P%s' % l].fill = yellow 
        l = l+2
        #moyenne
        ws['J%s' % l] = 'Moyenne'
        ws['K%s' % l] = ws['P%s' % str(l-4)].value
        #ecart_type
        l+=1
        ws['J%s' % l] = 'Ecart Type'
        ws['K%s' % l] = ws['P%s' % str(l-4)].value
        
        #LCS
        l+=1
        ws['J%s' % l] = 'LCS'
        # ws['K%s' % l] = '=K%s'% str(l-2) +'+'+'('+'3*'+'K%s'% str(l-1) +')'
        # ws['K%s' % l] = '=_xlfn.(K%s+(3*K%s))'%(str(l-2),str(l-1))
        ws['K%s' % l] = '=(K%s+(3*K%s))'%(str(l-2),str(l-1))
        # lcs = moyenne + (3*ecartype)

        #LCI
        l+=1
        ws['J%s' % l] = 'LCI'
        # ws['K%s' % l] = '=K%s'% str(l-3) +'-'+'('+'3*'+'K%s'% str(l-2) +')'
        # ws['K%s' % l] = '=_xlfn.(K%s-(3*K%s))'%(str(l-3),str(l-2))
        ws['K%s' % l] = '=(K%s-(3*K%s))'%(str(l-3),str(l-2))
        
        #DEP Cible
        l+=1
        ws['J%s' % l] = 'DEP Cible'
        ws['K%s' % l] = self.takt
        
        # LSS
        l+=1
        ws['J%s' % l] = 'LSS'
        ws['K%s' % l] = self.lss

        #CPK
        l+=1
        ws['J%s' % l] = 'CPK'       
        # ws['K%s' % l] = '='+'('+'K%s'% str(l-1) +'-'+'K%s'%(l-6)+')'+'/'+'('+'3*'+'K%s'% str(l-5) +')'
        ws['K%s' % l] = '=((K%s-K%s)/(3*K%s))'%(str(l-1),str(l-6),str(l-5))
        # ws['K%s' % l] = (lss - moyenne)/(3*ecartype)
        
        for ligne in range (1,l+1):
            ws['B%s'% ligne].font = ft
            ws['C%s'% ligne].font = ft
            ws['D%s'% ligne].font = ft
            ws['E%s'% ligne].font = ft
            ws['F%s'% ligne].font = ft
            ws['G%s'% ligne].font = ft
            ws['H%s'% ligne].font = ft
            if ws['I%s'% ligne].value in ['MOYENNE','ECART-TYPE','OBJECTIF','Step 1','Step 2','Step 3','Step 4','Step 5','Step 6','clôture/j calendaires']:
                ws['I%s'% ligne].font = ft_entete
            else:
                ws['I%s'% ligne].font = ft

            ws['J%s'% ligne].font = ft
            ws['K%s'% ligne].font = ft
            ws['L%s'% ligne].font = ft
            ws['M%s'% ligne].font = ft
            ws['N%s'% ligne].font = ft
            ws['O%s'% ligne].font = ft
            ws['P%s'% ligne].font = ft
            ws['Q%s'% ligne].font = ft
            ws['R%s'% ligne].font = ft
            ws['S%s'% ligne].font = ft
            ws['T%s'% ligne].font = ft
            ws['U%s'% ligne].font = ft
            ws['V%s'% ligne].font = ft
            ws['W%s'% ligne].font = ft
            ws['X%s'% ligne].font = ft
        for ligne in range (1,l-10):
            ws['A%s'% ligne].border = border
            ws['B%s'% ligne].border = border
            ws['C%s'% ligne].border = border
            ws['D%s'% ligne].border = border
            ws['E%s'% ligne].border = border
            ws['F%s'% ligne].border = border
            ws['G%s'% ligne].border = border
            ws['H%s'% ligne].border = border
            ws['I%s'% ligne].border = border
            ws['J%s'% ligne].border = border
            ws['K%s'% ligne].border = border
            ws['L%s'% ligne].border = border
            ws['M%s'% ligne].border = border
            ws['N%s'% ligne].border = border
            ws['O%s'% ligne].border = border
            ws['P%s'% ligne].border = border

        for cell in ['A1','B2','C2','D2','B3','C3','D3','E3','F3','G3','H3','I3','Q3','R3','S3','T3','U3','V3','W3','X3','J2','K2','L2','M2','N2','O2']:
            ws[cell].font = ft_entete
               
        for cell in range(l,l-9):
            ws['P%s'% cell].font = ft_entete
            ws['P%s'% cell].fill = yellow
        
        for moy in range(4,l-10):
            ws['S%s'%moy].value = '=K%s'% str(l-6)
            ws['T%s'%moy].value = '=K%s'% str(l-5)
            ws['U%s'%moy].value = '=K%s'% str(l-4)
            ws['V%s'%moy].value = '=K%s'% str(l-3)
            ws['W%s'%moy].value = '=K%s'% str(l-2)
            ws['X%s'%moy].value = '=K%s'% str(l-1)

        wb.save("%s/moyenne.xlsx" % tmpdir)
        with open("%s/moyenne.xlsx" % tmpdir, "rb") as xls_file:
            encoded_string = base64.b64encode(xls_file.read())
        
        self.moyenne_name="MOYENNE.xlsx"
        self.moyenne_file = encoded_string
        return {
            'name' : 'Moyenne Duree Etape',
            'context' : self._context,
            'view_type' : 'form',
            'view_mode' : 'form',
            'res_model' : 'star.step.average',
            'type' : 'ir.actions.act_window',
            'target' : 'new',
            'res_id' : self[0].id,
        }