# -*- coding: utf-8 -*-
{
    'name': "Import Employee, Prime",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Nexthope",
    'website': "http://www.nexthope.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','base_import','hr_attendance'],

    # always loaded
    'data': [
    ],
    # only loaded in demonstration mode
    'demo': [

    ],
}
