# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import datetime
from datetime import datetime
from datetime import timedelta
# import time

from odoo import api, fields, models, tools, SUPERUSER_ID
from odoo.tools.translate import _
from odoo.exceptions import UserError
import binascii
import tempfile
import io
import sys
import unicodedata
try:
    import csv
except ImportError:
    _logger.debug('Cannot `import csv`.')
try:
    import cStringIO
except ImportError:
    _logger.debug('Cannot `import cStringIO`.')
try:
    import base64
except ImportError:
    _logger.debug('Cannot `import base64`.')

class HrApplicant(models.Model):
    _inherit = 'hr.applicant'

    date_action = fields.Datetime("Next Action Date")


class ImportCandidature(models.TransientModel):
    _name = 'import.candidature.wizard'

    reload(sys)
    sys.setdefaultencoding("iso-8859-1")


    file = fields.Binary('Fichier')

    @api.multi
    def import_candidature_wizard(self):
        obj_applicant = self.env['hr.applicant']
        obj_recruitment_stage = self.env['hr.recruitment.stage']
        obj_job = self.env['hr.job']
        obj_test_test = self.env['test.test']
        obj_partner = self.env['res.partner']
        obj_user = self.env['res.users']
        obj_stage_history = self.env['stage.history']

        print "RAKOTO EST ICII"
        ######################## FIRST STEP OPEN THE FILE ##################
        #Encodage en base 64
        csv_data = base64.b64decode(self.file)

        data_file = cStringIO.StringIO(csv_data)

        data_file.seek(0)
        ###################### SECOND STEP READ THE FILE AND LOOP##########
        csv_reader = csv.reader(data_file, delimiter=';')

#####################RECUPERATION DES LIGNES DE CANDIDATURE################
        data = []
        for row in csv_reader:
            data.append(row)
        # print "########################JE SUIS L'ENSEMBLE DU DATA##############"
        data = data[2:]
        # print "########################JE SUIS LA FIN DE L'ENSEMBLE DU DATA##############"
        vals_test_ids = []
        vals_applicant = []
        vals_test_test = []
        vals_history = []
        vals_job = []
        ######################## RECUPERATION DE LA LIGNE ########
        for applicant in data:
            commentaire = applicant[36]
            ######Date d'entree######
            date_open = applicant[5].replace(' ','')
            if date_open:
                date_open = datetime.strptime(date_open,"%d/%m/%Y").strftime("%Y-%m-%d %H:%M")

######################################## CREATION LIGNE TESTS TESTS CANDIDATURES ####################################################
    # ##### IF RESULTAT TE : TEST ECRIT, IF RESULTAT TT: TEST TECHNIQUE ########
            date_test_ecrit = str(applicant[6]).replace(' ','')

            heure_test = applicant[7].replace('h',':').replace('H',':').replace('-','').replace(' ','')
            if heure_test and len(heure_test) != 5:

                if len(heure_test) == 4:
                    heure_test = '0'+heure_test
                if len (heure_test) == 3:
                    heure_test = heure_test+'00'
                if len(heure_test) == 2:
                    heure_test = '0'+heure_test+'00'

            if date_test_ecrit and heure_test:
                date_test_ecrit = date_test_ecrit+' '+heure_test
                date_test_ecrit = datetime(int(date_test_ecrit[6:10]),int(date_test_ecrit[3:5]),int(date_test_ecrit[0:2]),int(date_test_ecrit[11:13]),int(date_test_ecrit[14:16]))
                date_test_ecrit = date_test_ecrit - timedelta(hours=3)


            lieu_test = applicant[8]
            resultat_test_ecrit = str(applicant[9]).upper()
            resultat_test_technique = str(applicant[10]).upper()


############################# SI TEST ECRIT#####################################################

            if date_test_ecrit or heure_test or lieu_test or resultat_test_ecrit:
                if resultat_test_ecrit:
                    stage_id = 4
                    name_for_test = 'TEST ECRIT'
                    vals_test_ids.append((0,0,{
                        'name': name_for_test,
                        'applicant_id':obj_applicant.id,
                        'resultat':resultat_test_ecrit,
                        'commentaire':commentaire,
                        'date':  False if date_test_ecrit == '' else date_test_ecrit,
                        'lieu':lieu_test,
                        'step':stage_id,
                    }))

                    vals_history.append({
                    'name':name_for_test,
                    'date': False if date_test_ecrit == '' else date_test_ecrit,
                    'applicant_id':obj_applicant.id,
                    'test':name_for_test+' => '+resultat_test_ecrit,
                    'step':stage_id,
                    'commentaire':commentaire,
                    })

                if resultat_test_technique:
                    stage_id = 4
                    name_for_test = 'TEST TECHNIQUE'
                    vals_test_ids.append((0,0,{
                        'name': name_for_test,
                        'applicant_id':obj_applicant.id,
                        'resultat':resultat_test_technique,
                        'commentaire':commentaire,
                        'date':  False if date_test_ecrit == '' else date_test_ecrit,
                        'lieu':lieu_test,
                        'step':stage_id,
                    }))

                    vals_history.append({
                    'name':name_for_test,
                    'date': False if date_test_ecrit == '' else date_test_ecrit,
                    'applicant_id':obj_applicant.id,
                    'test':name_for_test+' => '+resultat_test_technique,
                    'step':stage_id,
                    'commentaire':commentaire,
                    })

######################################## IF ENTRETIEN RH##############

            heure_entretien_rh = applicant[12].replace('h',':').replace('H',':').replace('-','').replace(' ','')
            if heure_entretien_rh and len(heure_entretien_rh) != 5:
                if len(heure_entretien_rh) == 4:
                    heure_entretien_rh = '0'+heure_entretien_rh
                if len (heure_entretien_rh) == 3:
                    heure_entretien_rh = heure_entretien_rh+'00'
                if len(heure_entretien_rh) == 2:
                    heure_entretien_rh = '0'+heure_entretien_rh+'00'

            date_entretien_rh = applicant[11].replace(' ','')
            if date_entretien_rh and heure_entretien_rh:
                date_entretien_rh = date_entretien_rh+' '+heure_entretien_rh
                date_entretien_rh = datetime(int(date_entretien_rh[6:10]),int(date_entretien_rh[3:5]),int(date_entretien_rh[0:2]),int(date_entretien_rh[11:13]),int(date_entretien_rh[14:16]))
                date_entretien_rh = date_entretien_rh - timedelta(hours=3)

            lieu_entretien_rh = str(applicant[13])
            resultat_entretien_rh = str(applicant[14]).upper()

            if date_entretien_rh or lieu_entretien_rh or resultat_entretien_rh:
                name_for_test = 'ENTRETIEN RH'
                stage_id = 17
                vals_test_ids.append((0,0,{
                    'name': name_for_test,
                    'applicant_id':obj_applicant.id,
                    'resultat':resultat_entretien_rh,
                    'commentaire':commentaire,
                    'date':  False if date_entretien_rh == '' else date_entretien_rh,
                    'lieu':lieu_entretien_rh,
                    'step':stage_id,
                }))
                vals_history.append({
                'name':name_for_test,
                'date': False if date_entretien_rh == '' else date_entretien_rh,
                'applicant_id':obj_applicant.id,
                'test':name_for_test+' => '+resultat_entretien_rh,
                'step':stage_id,
                'commentaire':commentaire,
                })

    # ########################### IF ENTRETIEN RR ##############
            heure_entretien_rr = applicant[16].replace('h',':').replace('H',':').replace('-','').replace(' ','')
            if heure_entretien_rr and len(heure_entretien_rr) != 5:

                if len(heure_entretien_rr) == 4:
                    heure_entretien_rr = '0'+heure_entretien_rr
                if len (heure_entretien_rr) == 3:
                    heure_entretien_rr = heure_entretien_rr+'00'
                if len(heure_entretien_rr) == 2:
                    heure_entretien_rr = '0'+heure_entretien_rr+'00'

            date_entretien_rr = applicant[15].replace(' ','')

            if date_entretien_rr and heure_entretien_rr:
                date_entretien_rr = date_entretien_rr+' '+heure_entretien_rr
                date_entretien_rr = datetime(int(date_entretien_rr[6:10]),int(date_entretien_rr[3:5]),int(date_entretien_rr[0:2]),int(date_entretien_rr[11:13]),int(date_entretien_rr[14:16]))
                date_entretien_rr = date_entretien_rr - timedelta(hours=3)

            lieu_entretien_rr = applicant[17]
            resultat_entretien_rr = str(applicant[18]).upper()

            if date_entretien_rr or lieu_entretien_rr or resultat_entretien_rr:
                stage_id = 17
                name_for_test = 'ENTRETIEN RR'
                vals_test_ids.append((0,0,{
                    'name': name_for_test,
                    'applicant_id':obj_applicant.id,
                    'resultat':resultat_entretien_rr,
                    'commentaire':commentaire,
                    'date':  False if date_entretien_rr == '' else date_entretien_rr,
                    'lieu':lieu_entretien_rr,
                    'step':stage_id,
                }))
                vals_history.append({
                'name':name_for_test,
                'date': False if date_entretien_rr == '' else date_entretien_rr,
                'applicant_id':obj_applicant.id,
                'test':name_for_test+' => '+resultat_entretien_rr,
                'step':stage_id,
                'commentaire':commentaire,
                })
    # ######################## IF ENQUETE DE MORALITE ################################################
            resultat_em = str(applicant[19]).upper()
            if resultat_em:
                name_for_test = 'ENQUETE DE MORALITE'
                stage_id = 13
                vals_test_ids.append((0,0,{
                    'name': name_for_test,
                    'applicant_id':obj_applicant.id,
                    'resultat':resultat_em,
                    'commentaire':commentaire,
                    'step':stage_id,
                }))
                vals_history.append({
                'name':name_for_test,
                'applicant_id':obj_applicant.id,
                'test':name_for_test+' => '+resultat_em,
                'step':stage_id,
                'commentaire':commentaire,
                })

    # ######################## IF TEST DE CONDUITE #####################################################
            heure_test_conduite = applicant[21].replace('h',':').replace('H',':').replace('-','').replace(' ','')

            if heure_test_conduite and len(heure_test_conduite) != 5:
                if len(heure_test_conduite) == 4:
                    heure_test_conduite = '0'+heure_test_conduite
                if len (heure_test_conduite) == 3:
                    heure_test_conduite = heure_test_conduite+'00'
                if len(heure_test_conduite) == 2:
                    heure_test_conduite = '0'+heure_test_conduite+'00'

            date_test_conduite = applicant[20].replace(' ','')

            if date_test_conduite and heure_test_conduite:
                date_test_conduite = date_test_conduite+' '+heure_test_conduite
                date_test_conduite = datetime(int(date_test_conduite[6:10]),int(date_test_conduite[3:5]),int(date_test_conduite[0:2]),int(date_test_conduite[11:13]),int(date_test_conduite[14:16]))
                date_test_conduite = date_test_conduite - timedelta(hours=3)

            lieu_test_conduite = applicant[22]
            responsable_test_conduite = applicant[23]
            resultat_test_conduite = str(applicant[24]).upper()

            if date_test_conduite or lieu_test_conduite or resultat_test_conduite:
                stage_id = 15
                name_for_test = 'TEST DE CONDUITE'
                vals_test_ids.append((0,0,{
                    'name': name_for_test,
                    'applicant_id':obj_applicant.id,
                    'resultat':resultat_test_conduite,
                    'commentaire':commentaire,
                    'date':  False if date_test_conduite == '' else date_test_conduite,
                    'lieu':lieu_test_conduite,
                    'step':stage_id,
                }))
                vals_history.append({
                'name':name_for_test,
                'applicant_id':obj_applicant.id,
                'test':name_for_test+' => '+resultat_test_conduite,
                'step':stage_id,
                'commentaire':commentaire,
                'date': False if date_test_conduite == '' else date_test_conduite,
                'acteur':responsable_test_conduite
                })
    # ######################## IF ENT N+1 #######################
            heure_entun = applicant[26].replace('h',':').replace('H',':').replace('-','').replace(' ','')
            if heure_entun and len(heure_entun) != 5:
                if len(heure_entun) == 4:
                    heure_entun = '0'+heure_entun
                if len (heure_entun) == 3:
                    heure_entun = heure_entun+'00'
                if len(heure_entun) == 2:
                    heure_entun = '0'+heure_entun+'00'

            date_entun = applicant[25].replace(' ','')

            if date_entun and heure_entun:
                date_entun = date_entun+' '+heure_entun
                date_entun = datetime(int(date_entun[6:10]),int(date_entun[3:5]),int(date_entun[0:2]),int(date_entun[11:13]),int(date_entun[14:16]))
                date_entun = date_entun - timedelta(hours=3)

            lieu_entun = applicant[27]
            acteur_entun = applicant[28]
            resultat_entun = str(applicant[29]).upper()

            if date_entun or lieu_entun or resultat_entun:
                name_for_test = 'ENTRETIEN N+1'
                stage_id = 14
                vals_test_ids.append((0,0,{
                    'name': name_for_test,
                    'applicant_id':obj_applicant.id,
                    'resultat':resultat_entun,
                    'commentaire':commentaire,
                    'date': False if date_entun == '' else date_entun,
                    'lieu':lieu_entun,
                    'step':stage_id,
                    'acteur':acteur_entun
                }))
                vals_history.append({
                'name':name_for_test,
                'applicant_id':obj_applicant.id,
                'test':name_for_test+' => '+resultat_entun,
                'date':  False if date_entun == '' else date_entun,
                'step':stage_id,
                'commentaire':commentaire,
                'acteur':acteur_entun
                })

    # ######################### IF ENT N+2 ######################
            heure_entdeux = applicant[31].replace('h',':').replace('H',':').replace('-','').replace(' ','')
            if heure_entdeux and len(heure_entdeux) != 5:
                if len(heure_entdeux) == 4:
                    heure_entdeux = '0'+heure_entdeux
                if len (heure_entdeux) == 3:
                    heure_entdeux = heure_entdeux+'00'
                if len(heure_entdeux) == 2:
                    heure_entdeux = '0'+heure_entdeux+'00'
            date_entdeux = applicant[30].replace(' ','')

            if date_entdeux and heure_entdeux:
                date_entdeux = date_entdeux+' '+heure_entdeux
                date_entdeux = datetime(int(date_entdeux[6:10]),int(date_entdeux[3:5]),int(date_entdeux[0:2]),int(date_entdeux[11:13]),int(date_entdeux[14:16]))
                date_entdeux = date_entdeux - timedelta(hours=3)

            lieu_entdeux = applicant[32]
            acteur_entdeux = applicant[33]
            resultat_entdeux = str(applicant[34]).upper()

            if date_entdeux or lieu_entdeux or resultat_entdeux:
                name_for_test = 'ENTRETIEN N+2'
                stage_id = 14
                vals_test_ids.append((0,0,{
                    'name': name_for_test,
                    'applicant_id':obj_applicant.id,
                    'resultat':resultat_entdeux,
                    'commentaire':commentaire,
                    'acteur':acteur_entdeux,
                    'date':  False if date_entdeux == '' else date_entdeux,
                    'lieu':lieu_entdeux,
                    'step':stage_id,
                    'acteur':acteur_entdeux
                }))
                vals_history.append({
                'name':name_for_test,
                'applicant_id':obj_applicant.id,
                'test':name_for_test+' => '+resultat_entdeux,
                'date':  False if date_entdeux == '' else date_entdeux,
                'step':stage_id,
                'commentaire':commentaire,
                'acteur':acteur_entdeux
                })
     ##########################STATUT CANDIDAT##########
     ######################### ###########################

            etat_candidature = applicant[35]
            if etat_candidature and etat_candidature == 'CANDIDATURE NON RETENUE':
                stage_id = 2
            if etat_candidature and etat_candidature == 'CANDIDATURE RETENUE':
                stage_id = 12
            if etat_candidature and etat_candidature == 'CANDIDATURE REÇUE':
                stage_id = 16
            if etat_candidature and etat_candidature == 'VIVIER':
                stage_id = 3

    ####################### ##############################
            #############################CREATION DE LA CANDIDATURE#########################

            applicant_name = str(applicant[2])
            user_name = str(applicant[1])

            job_name = unicode(applicant[3],'iso-8859-1').upper().replace('Œ','OE')

            ## Recherche si partner = user_name
            partner_id = obj_partner.search([('name','ilike',user_name)],order='id ASC')
            # import pudb; pudb.set_trace()

            partner = False
            if len(partner_id):
                partner  = partner_id[0].id
                # import pudb; pudb.set_trace()

            if not partner:
                vals_partner = {
                                'name':user_name,
                                'active':True,
                                'supplier':False,
                                'employee':False,
                                'is_company':False,
                                'partner_share':False,
                                'customer':True,
                                }

                partner_id = obj_partner.create(vals_partner)
                partner = partner_id.id

            job_id =  obj_job.search([('name','ilike',job_name)],order='id ASC')

            job = False

            if len(job_id):
                job = job_id[0].id
            ## RECHERCHE JOB CORRESPONDANT EN CREER S'IL n'y en a pas
                # import pudb; pudb.set_trace()

            if not job:
                vals_job = {
                                'company_id':1,
                                'state':'recruit',
                                'name':job_name,
                                'user_id':False,
                                'write_date': datetime.now(),
                                'alias_id':self.env['mail.alias'].search([('alias_parent_thread_id','=',job)]).id
                            }

                job_id = obj_job.create(vals_job)
                job = job_id.id

    ########## RECHERCHER ID CORRESPONDANT AU PARTNER_ID ############
            user_id = obj_user.search([('partner_id','=',partner)])

            vals_applicant = {
                'name':applicant_name,
                'user_id':user_id.id,
                'job_id':job,
                'color':0,
                'partner_name':applicant_name,
                'stage_id':stage_id,
                'active': False,
                'last_stage_id':stage_id,
                'date_open': datetime.now() if not date_open else date_open,
                'test_ids':vals_test_ids
            }
##################################################CREATION CANDIDATURE#######################################################################
            applicant = obj_applicant.create(vals_applicant)
############################# CREATION DANS STAGE_HISTORY #######################
            for vals in vals_history:
                vals['applicant_id'] = applicant.id
                vals['name'] = applicant.name
                obj_stage_history.create(vals)

        #####VIDE LA LISTE test_ids et stage.history a chaque fin d'iteration#######
            vals_test_ids = []
            vals_history = []
