# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from datetime import date, time, timedelta, datetime

class StageHistory(models.Model):
    _name = 'stage.history'
    _order = 'date desc'

    name = fields.Char(readonly=True)
    step = fields.Many2one(string="Etape", comodel_name="hr.recruitment.stage", readonly=True)
    applicant_id = fields.Many2one(string="Candidature", comodel_name="hr.applicant", readonly=True)
    date = fields.Datetime(string="Date", readonly=True)
    test = fields.Char(string="test", readonly=True)
    commentaire = fields.Char(string="Commentaire",readonly=True)

class TestTest(models.Model):
    _name = 'test.test'

    name = fields.Char(string="Libellé")
    date = fields.Datetime(string="Date")
    resultat = fields.Selection(
        string="Resultat",
        selection=[
                ('OK', 'OK'),
                ('NON', 'NON'),
                ('ABS', 'ABS'),
                ('INJ', 'INJ'),
                ('MOYEN','MOYEN'),
                ('RETENUE','RETENUE'),
                ('NON RETENUE','NON RETENUE'),
                ('RETENU','RETENU'),
                ('NON RETENU','NON RETENU'),
                ('X','X'),
                ('FAIT','FAIT'),
                ('A FAIRE','A FAIRE'),
                ('EXCEPTIONNEL','EXCEPTIONNEL'),
                ('A ORIENTER','A ORIENTER'),
                ('NON ','NON '),
                ('PERFECTIBLE','PERFECTIBLE'),
                ('A PROGRAMMER','A PROGRAMMER'),
                ('ENQUETE DE MORALITE','ENQUETE DE MORALITE'),
                (' ',' '),
                ('  ','  ',),
                ('ATTENTE RETOUR','ATTENTE RETOUR'),
                ('A RELANCER','A RELANCER')
        ],
    )
    applicant_id = fields.Many2one(string="Candidature", comodel_name="hr.applicant")
    lieu = fields.Char(string="Lieu")
    acteur = fields.Char(string="Acteur")
    commentaire = fields.Char(string="Commentaire")

class HrRecruitment(models.Model):
    _inherit = 'hr.applicant'

    history_id = fields.Many2one(string="Historique", comodel_name="stage.history", compute='_compute_history_id')
    history_count = fields.Integer(compute='_compute_history_count', string='Historique')
    test_ids = fields.One2many(string="", comodel_name="test.test", inverse_name="applicant_id")

    seq = fields.Integer(string="SEQ")
    expl = fields.Char(string="Expl")
    etape = fields.Char(string="etape")
    poste = fields.Char(string="poste")
    responsable = fields.Char(string="responsable")
    source = fields.Char(string="source")
    parrain = fields.Char(string="parrain")
    # test_test = fields.Integer(compute='_compute_test_calcul',string="test_test")
    
    

    def _compute_history_count(self):
        # read_group as sudo, since history count is displayed on form view
        history_data = self.env['stage.history'].sudo().read_group([('applicant_id', 'in', self.ids)], ['applicant_id'], ['applicant_id'])
        result = dict((data['applicant_id'][0], data['applicant_id_count']) for data in history_data)
        for applicant in self:
            applicant.history_count = result.get(applicant.id, 0)


    def _compute_history_id(self):
        """ get the lastest contract """
        history = self.env['stage.history']
        for applicant in self:
            applicant.history_id = history.search([('applicant_id', '=', applicant.id)], order='date desc', limit=1)
            
    # def _compute_test_calcul(self):
    #     self.test_test = 3
    #     # return ['rakoto':3]
        

        
    

    def _onchange_stage_id_internal(self, stage_id):
        history = self.env['stage.history']
        tests = self.test_ids
        test = []
        test0 = ""
        test1 = ""
        commentaire = ""
        if self.test_ids:
            for line in self.test_ids:
                test.append((line.date,line.name,line.resultat,line.commentaire))
                # import pudb; pudb.set_trace()
            test = sorted(test , key=lambda elem: elem[0], reverse=True)
            # import pudb; pudb.set_trace()
            test0 = test[0][1]
            test1 = test[0][2]
            commentaire = test[0][3]



        vals = {
            'name' : self.name,
            'date' : datetime.today(),
            'step' : stage_id,
            'applicant_id' : self.id,
            'test' : test0 + ' => ' + test1,
            'commentaire': commentaire,
            }
        history.create(vals)

        return super(HrRecruitment, self)._onchange_stage_id_internal(stage_id)
