# -*- coding: utf-8 -*-

from odoo import models, fields, api

class HrRecruitmentStage(models.Model):
    _inherit = 'hr.recruitment.stage'
    
    optimal_duration = fields.Integer(string=u'Duree optimale de traitement')
    duration_soon = fields.Integer(string=u'Duree acceptable de traitement')
    