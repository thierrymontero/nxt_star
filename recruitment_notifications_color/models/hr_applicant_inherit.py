# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from datetime import date, time, timedelta, datetime

class HrApplicant(models.Model):
    _inherit = 'hr.applicant'
    _order = 'create_date'
    
    
    @api.depends('date_open','date_preselection','date_recruitment_test','date_conv_te','date_test_ok','date_cas_injoignable','date_entretien_rh','date_conv_ent','date_rh_ok','date_morality_survey','date_affecting_test','date_entretien_n1','date_conv_ent_n1','date_successful_application','date_viver','date_unsuccessful_application')
    def _compute_duration(self):
        for record in self:
            if (record.stage_id.id):
                
                if record.date_open:
                    duration_days = datetime.now() - datetime.strptime(record.date_open, "%Y-%m-%d  %H:%M:%S")
                    record.days_durations = duration_days.days
                    calculate_date = datetime.now() - datetime.strptime(record.date_open, "%Y-%m-%d  %H:%M:%S")
                    record.diffdate_open_now = calculate_date.days
                
                if record.date_open:
                    duration_received = datetime.now() - datetime.strptime(record.date_open, "%Y-%m-%d  %H:%M:%S")
                    record.candidature_received_durations = duration_received.days
                
                if record.date_preselection:
                    duration_preselection = datetime.now() - datetime.strptime(record.date_preselection, "%Y-%m-%d  %H:%M:%S")
                    record.preselection_durations = duration_preselection.days
                    
                
                if record.date_recruitment_test:
                    duration_recruitment_test = datetime.now() - datetime.strptime(record.date_recruitment_test, "%Y-%m-%d  %H:%M:%S")
                    record.recruitment_test_durations = duration_recruitment_test.days
                    
                
                if record.date_conv_te:
                    duration_conv_te = datetime.now() - datetime.strptime(record.date_conv_te, "%Y-%m-%d  %H:%M:%S")
                    record.conv_te_durations = duration_conv_te.days
                    
                if record.date_test_ok:
                    duration_test_ok = datetime.now() - datetime.strptime(record.date_test_ok, "%Y-%m-%d  %H:%M:%S")
                    record.test_ok_durations = duration_test_ok.days
                    
                
                if record.date_cas_injoignable:
                    duration_cas_injoignable = datetime.now() - datetime.strptime(record.date_cas_injoignable, "%Y-%m-%d  %H:%M:%S")
                    record.cas_injoignable_durations = duration_cas_injoignable.days
                    
                if record.date_entretien_rh:
                    duration_entretien_rh = datetime.now() - datetime.strptime(record.date_entretien_rh, "%Y-%m-%d  %H:%M:%S")
                    record.entretien_rh_durations = duration_entretien_rh.days
                    
                
                if record.date_conv_ent:
                    duration_conv_ent = datetime.now() - datetime.strptime(record.date_conv_ent, "%Y-%m-%d  %H:%M:%S")
                    record.conv_ent_durations = duration_conv_ent.days
                    
                
                if record.date_rh_ok:
                    duration_rh_ok = datetime.now() - datetime.strptime(record.date_rh_ok, "%Y-%m-%d  %H:%M:%S")
                    record.rh_ok_durations = duration_rh_ok.days
                    
                
                if record.date_morality_survey:
                    duration_morality_survey = datetime.now() - datetime.strptime(record.date_morality_survey, "%Y-%m-%d  %H:%M:%S")
                    record.morality_survey_durations = duration_morality_survey.days
                    
                
                if record.date_affecting_test:
                    duration_affecting_test = datetime.now() - datetime.strptime(record.date_affecting_test, "%Y-%m-%d  %H:%M:%S")
                    record.affecting_test_durations = duration_affecting_test.days
                    
                
                if record.date_entretien_n1:
                    duration_entretien_n1 = datetime.now() - datetime.strptime(record.date_entretien_n1, "%Y-%m-%d  %H:%M:%S")
                    record.entretien_n1_durations = duration_entretien_n1.days
                    
                
                if record.date_conv_ent_n1:
                    duration_conv_ent_n1 = datetime.now() - datetime.strptime(record.date_conv_ent_n1, "%Y-%m-%d  %H:%M:%S")
                    record.conv_ent_n1_durations = duration_conv_ent_n1.days
                    
                
                if record.date_successful_application:
                    duration_successful_application = datetime.now() - datetime.strptime(record.date_successful_application, "%Y-%m-%d  %H:%M:%S")
                    record.successful_application_durations = duration_successful_application.days
                    
                
                if record.date_viver:
                    duration_viver = datetime.now() - datetime.strptime(record.date_viver, "%Y-%m-%d  %H:%M:%S")
                    record.viver_durations = duration_viver.days
                    
                
                if record.date_unsuccessful_application:
                    duration_unsuccessful_application = datetime.now() - datetime.strptime(record.date_unsuccessful_application, "%Y-%m-%d  %H:%M:%S")
                    record.unsuccessful_application_durations = duration_unsuccessful_application.days
                    
                
    
    date_received = fields.Datetime(string=u'Date de début',readonly=True)
    
    date_preselection = fields.Datetime(string=u'Date preselection ',readonly=True)
    
    date_recruitment_test = fields.Datetime(string=u'Date recrutement test',readonly=True)
    
    date_conv_te = fields.Datetime(string=u'Date conv te',readonly=True)
    
    date_test_ok = fields.Datetime(string=u'Date test ok',readonly=True)
    
    date_cas_injoignable = fields.Datetime(string=u'Date cas injoignable',readonly=True)
    
    date_entretien_rh = fields.Datetime(string=u'Date entretien rh',readonly=True)
    
    date_conv_ent = fields.Datetime(string=u'Date conv ent',readonly=True)
    
    date_rh_ok = fields.Datetime(string=u'Date rh ok',readonly=True)
    
    date_morality_survey = fields.Datetime(string=u'Date enquete de moralite',readonly=True)
    
    date_affecting_test = fields.Datetime(string=u'Date test conduite',readonly=True)
    
    date_entretien_n1 = fields.Datetime(string=u'Date entretien n+1',readonly=True)
    
    date_conv_ent_n1 = fields.Datetime(string=u'Date conv ent n+1',readonly=True)
    
    date_successful_application = fields.Datetime(string=u'Date candidature retenue',readonly=True)
    
    date_viver = fields.Datetime(string=u'Date viver',readonly=True)
    
    date_unsuccessful_application = fields.Datetime(string=u'Date candidature non retenue',readonly=True)
    
    date_vide = fields.Datetime(string=u'Date de début',readonly=True)
    
    
    
    diffdate_open_now = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    days_ok = fields.Boolean(compute='_compute_calculate_days', string="test",default =False)
    days_due_soon = fields.Boolean(compute='_compute_calculate_days', string="test",default =False)
    days_late = fields.Boolean(compute='_compute_calculate_days', string="test",default =False)
    days = fields.Integer(compute='_compute_calculate_days', string="test OK 2",default =False)
    days_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    
    
    candidature_received_ok = fields.Boolean(compute='_compute_calculate_reminder', string="test",default =False)
    candidature_received_due_soon = fields.Boolean(compute='_compute_calculate_reminder', string="test",default =False)
    candidature_received_late = fields.Boolean(compute='_compute_calculate_reminder', string="test",default =False)
    candidature_received = fields.Integer(compute='_compute_calculate_reminder', string="test OK 2",default =False)
    candidature_received_test = fields.Boolean(compute='_compute_calculate_reminder', string="test",default =False)
    candidature_received_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    preselection_ok = fields.Boolean(compute='_compute_calculate_preselection', string="test",default =False)
    preselection_due_soon = fields.Boolean(compute='_compute_calculate_preselection', string="test",default =False)
    preselection_late = fields.Boolean(compute='_compute_calculate_preselection', string="test",default =False)
    preselection = fields.Integer(compute='_compute_calculate_preselection', string="test OK 2",default =False)
    preselection_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    recruitment_test_ok = fields.Boolean(compute='_compute_calculate_recruitment_test', string="test",default =False)
    recruitment_test_due_soon = fields.Boolean(compute='_compute_calculate_recruitment_test', string="test",default =False)
    recruitment_test_late = fields.Boolean(compute='_compute_calculate_recruitment_test', string="test",default =False)
    recruitment_test = fields.Integer(compute='_compute_calculate_recruitment_test', string="test OK 2",default =False)
    recruitment_test_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    conv_te_ok = fields.Boolean(compute='_compute_calculate_conv_te', string="test",default =False)
    conv_te_due_soon = fields.Boolean(compute='_compute_calculate_conv_te', string="test",default =False)
    conv_te_late = fields.Boolean(compute='_compute_calculate_conv_te', string="test",default =False)
    conv_te = fields.Integer(compute='_compute_calculate_conv_te', string="test OK 2",default =False)
    conv_te_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    test_ok_ok = fields.Boolean(compute='_compute_calculate_test_ok', string="test",default =False)
    test_ok_due_soon = fields.Boolean(compute='_compute_calculate_test_ok', string="test",default =False)
    test_ok_late = fields.Boolean(compute='_compute_calculate_test_ok', string="test",default =False)
    test_ok = fields.Integer(compute='_compute_calculate_test_ok', string="test OK 2",default =False)
    test_ok_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    cas_injoignable_ok = fields.Boolean(compute='_compute_calculate_cas_injoignable', string="test",default =False)
    cas_injoignable_due_soon = fields.Boolean(compute='_compute_calculate_cas_injoignable', string="test",default =False)
    cas_injoignable_late = fields.Boolean(compute='_compute_calculate_cas_injoignable', string="test",default =False)
    cas_injoignable = fields.Integer(compute='_compute_calculate_cas_injoignable', string="test OK 2",default =False)
    cas_injoignable_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    entretien_rh_ok = fields.Boolean(compute='_compute_calculate_entretien_rh', string="test",default =False)
    entretien_rh_due_soon = fields.Boolean(compute='_compute_calculate_entretien_rh', string="test",default =False)
    entretien_rh_late = fields.Boolean(compute='_compute_calculate_entretien_rh', string="test",default =False)
    entretien_rh = fields.Integer(compute='_compute_calculate_entretien_rh', string="test OK 2",default =False)
    entretien_rh_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    conv_ent_ok = fields.Boolean(compute='_compute_calculate_conv_ent', string="test",default =False)
    conv_ent_due_soon = fields.Boolean(compute='_compute_calculate_conv_ent', string="test",default =False)
    conv_ent_late = fields.Boolean(compute='_compute_calculate_conv_ent', string="test",default =False)
    conv_ent = fields.Integer(compute='_compute_calculate_conv_ent', string="test OK 2",default =False)
    conv_ent_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    rh_ok_ok = fields.Boolean(compute='_compute_calculate_rh_ok', string="test",default =False)
    rh_ok_due_soon = fields.Boolean(compute='_compute_calculate_rh_ok', string="test",default =False)
    rh_ok_late = fields.Boolean(compute='_compute_calculate_rh_ok', string="test",default =False)
    rh_ok = fields.Integer(compute='_compute_calculate_rh_ok', string="test OK 2",default =False)
    rh_ok_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    morality_survey_ok = fields.Boolean(compute='_compute_calculate_morality_survey', string="test",default =False)
    morality_survey_due_soon = fields.Boolean(compute='_compute_calculate_morality_survey', string="test",default =False)
    morality_survey_late = fields.Boolean(compute='_compute_calculate_morality_survey', string="test",default =False)
    morality_survey = fields.Integer(compute='_compute_calculate_morality_survey', string="test OK 2",default =False)
    morality_survey_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    affecting_test_ok = fields.Boolean(compute='_compute_calculate_affecting_test', string="test",default =False)
    affecting_test_due_soon = fields.Boolean(compute='_compute_calculate_affecting_test', string="test",default =False)
    affecting_test_late = fields.Boolean(compute='_compute_calculate_affecting_test', string="test",default =False)
    affecting_test = fields.Integer(compute='_compute_calculate_affecting_test', string="test OK 2",default =False)
    affecting_test_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    entretien_n1_ok = fields.Boolean(compute='_compute_calculate_entretien_n1', string="test",default =False)
    entretien_n1_due_soon = fields.Boolean(compute='_compute_calculate_entretien_n1', string="test",default =False)
    entretien_n1_late = fields.Boolean(compute='_compute_calculate_entretien_n1', string="test",default =False)
    entretien_n1 = fields.Integer(compute='_compute_calculate_entretien_n1', string="test OK 2",default =False)
    entretien_n1_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    conv_ent_n1_ok = fields.Boolean(compute='_compute_calculate_conv_ent_n1', string="test",default =False)
    conv_ent_n1_due_soon = fields.Boolean(compute='_compute_calculate_conv_ent_n1', string="test",default =False)
    conv_ent_n1_late = fields.Boolean(compute='_compute_calculate_conv_ent_n1', string="test",default =False)
    conv_ent_n1 = fields.Integer(compute='_compute_calculate_conv_ent_n1', string="test OK 2",default =False)
    conv_ent_n1_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    successful_application_ok = fields.Boolean(compute='_compute_calculate_successful_application', string="test",default =False)
    successful_application_due_soon = fields.Boolean(compute='_compute_calculate_successful_application', string="test",default =False)
    successful_application_late = fields.Boolean(compute='_compute_calculate_successful_application', string="test",default =False)
    successful_application = fields.Integer(compute='_compute_calculate_successful_application', string="test OK 2",default =False)
    successful_application_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    viver_ok = fields.Boolean(compute='_compute_calculate_viver', string="test",default =False)
    viver_due_soon = fields.Boolean(compute='_compute_calculate_viver', string="test",default =False)
    viver_late = fields.Boolean(compute='_compute_calculate_viver', string="test",default =False)
    viver = fields.Integer(compute='_compute_calculate_viver', string="test OK 2",default =False)
    viver_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    unsuccessful_application_ok = fields.Boolean(compute='_compute_calculate_unsuccessful_application', string="test",default =False)
    unsuccessful_application_due_soon = fields.Boolean(compute='_compute_calculate_unsuccessful_application', string="test",default =False)
    unsuccessful_application_late = fields.Boolean(compute='_compute_calculate_unsuccessful_application', string="test",default =False)
    unsuccessful_application = fields.Integer(compute='_compute_calculate_unsuccessful_application', string="test OK 2",default =False)
    unsuccessful_application_durations = fields.Integer(compute='_compute_duration', string="Durations",default =False)
    
    
    # ------------------------------compute date ----------------------------------------------------------------------------
    
            
            
    def _onchange_stage_id_date(self, stage_id):
        if not stage_id:
            return False
        stage = self.env['hr.recruitment.stage'].browse(stage_id)
        if stage_id == 18:
            return {'value': {'date_preselection': fields.datetime.now()}}
        if stage_id == 4:
            return {'value': {'date_recruitment_test': fields.datetime.now()}}
        if stage_id == 23:
            return {'value': {'date_conv_te': fields.datetime.now()}}
        if stage_id == 21:
            return {'value': {'date_test_ok': fields.datetime.now()}}
        if stage_id == 25:
            return {'value': {'date_cas_injoignable': fields.datetime.now()}}
        if stage_id == 17:
            return {'value': {'date_entretien_rh': fields.datetime.now()}}
        if stage_id == 24:
            return {'value': {'date_conv_ent': fields.datetime.now()}}
        if stage_id == 22:
            return {'value': {'date_rh_ok': fields.datetime.now()}}
        if stage_id == 13:
            return {'value': {'date_morality_survey': fields.datetime.now()}}
        if stage_id == 15:
            return {'value': {'date_affecting_test': fields.datetime.now()}}
        if stage_id == 14:
            return {'value': {'date_entretien_n1': fields.datetime.now()}}
        if stage_id == 26:
            return {'value': {'date_conv_ent_n1': fields.datetime.now()}}
        if stage_id == 12:
            return {'value': {'date_successful_application': fields.datetime.now()}}
        if stage_id == 3:
            return {'value': {'date_viver': fields.datetime.now()}}
        if stage_id == 2:
            return {'value': {'date_unsuccessful_application': fields.datetime.now()}}
        return {'value': {'date_closed': False}}
    
    
    
    @api.model
    def create(self, vals):
        if vals.get('department_id') and not self._context.get('default_department_id'):
            self = self.with_context(default_department_id=vals.get('department_id'))
        if vals.get('job_id') or self._context.get('default_job_id'):
            job_id = vals.get('job_id') or self._context.get('default_job_id')
            for key, value in self._onchange_job_id_internal(job_id)['value'].iteritems():
                if key not in vals:
                    vals[key] = value
        if vals.get('user_id'):
            vals['date_open'] = fields.Datetime.now()
        if 'stage_id' in vals:
            vals.update(self._onchange_stage_id_internal(vals.get('stage_id'))['value'])
            vals.update(self._onchange_stage_id_date(vals.get('stage_id'))['value'])
        return super(HrApplicant, self.with_context(mail_create_nolog=True)).create(vals)

    @api.multi
    def write(self, vals):
        # user_id change: update date_open
        if vals.get('user_id'):
            vals['date_open'] = fields.Datetime.now()
        # stage_id: track last stage before update
        if 'stage_id' in vals:
            vals['date_last_stage_update'] = fields.Datetime.now()
            vals.update(self._onchange_stage_id_internal(vals.get('stage_id'))['value'])
            vals.update(self._onchange_stage_id_date(vals.get('stage_id'))['value'])
            for applicant in self:
                vals['last_stage_id'] = applicant.stage_id.id
                res = super(HrApplicant, self).write(vals)
        else:
            res = super(HrApplicant, self).write(vals)
        return res
    
    # ------------------------------compute date --------------------------------------------------------
    
    def _compute_calculate_reminder(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_candidature_recu(record,record.candidature_received_ok,16)
            
    def _compute_calculate_recruitment_test(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_recruitment_test(record,record.recruitment_test,4)
            
    def _compute_calculate_preselection(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_preselection(record,record.preselection_ok,18)
            
    def _compute_calculate_conv_te(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_conv_te(record,record.conv_te_ok,23)
            
    def _compute_calculate_test_ok(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_test_ok(record,record.conv_te_ok,21)
            
    def _compute_calculate_cas_injoignable(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_cas_injoignable(record,record.conv_te_ok,25)
            
    def _compute_calculate_entretien_rh(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_entretien_rh(record,record.entretien_rh_ok,17)
            
    def _compute_calculate_conv_ent(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_conv_ent(record,record.conv_te_ok,24)
            
    def _compute_calculate_rh_ok(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_rh_ok(record,record.conv_te_ok,22)
            
    def _compute_calculate_morality_survey(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_morality_survey(record,record.conv_te_ok,13)
            
    def _compute_calculate_affecting_test(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_affecting_test(record,record.conv_te_ok,15)
            
    def _compute_calculate_entretien_n1(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_entretien_n1(record,record.conv_te_ok,14)
            
    def _compute_calculate_conv_ent_n1(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_conv_ent_n1(record,record.conv_te_ok,26)
            
    def _compute_calculate_successful_application(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_successful_application(record,record.conv_te_ok,12)
            
    def _compute_calculate_viver(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_viver(record,record.conv_te_ok,3)
            
    def _compute_calculate_unsuccessful_application(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_unsuccessful_application(record,record.conv_te_ok,2)
            
    def _compute_calculate_days(self):
        for record in self:
            record.ensure_one()
            self._get_calcul_candidature(record,record.conv_te_ok)
            
        
    def _get_calcul_candidature(self, record, x):
        # obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
        
         
        if(record.days_durations < 32):
            record.days_ok = True
            record.days_due_soon = False
            record.days_late = False
        
        elif(record.days_durations < 39):
            record.days_ok = False
            record.days_due_soon = True
            record.days_late = False

        else:
            record.days_ok = False
            record.days_due_soon = False
            record.days_late = True
            
    def _get_calcul_candidature_recu(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
        
        
        if (record.stage_id.id == 16):
            record.candidature_received = obj_rec.id
            
         
        if record.candidature_received_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.candidature_received_ok = True
            record.candidature_received_due_soon = False
            record.candidature_received_late = False
        
        elif record.candidature_received_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.candidature_received_ok = False
            record.candidature_received_due_soon = True
            record.candidature_received_late = False

        else:
            record.candidature_received_ok = False
            record.candidature_received_due_soon = False
            record.candidature_received_late = True
            
            
    def _get_calcul_preselection(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 18):
            record.preselection = obj_rec.id
            
          
        if record.preselection_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.preselection_ok = True
            record.preselection_due_soon = False
            record.preselection_late = False
        
        elif record.preselection_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.preselection_ok = False
            record.preselection_due_soon = True
            record.preselection_late = False

        else:
            record.preselection_ok = False
            record.preselection_due_soon = False
            record.preselection_late = True
            
    def _get_calcul_recruitment_test(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
        
        
        if (record.stage_id.id == 4):
            record.recruitment_test = obj_rec.id
            
          
        if record.recruitment_test_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.recruitment_test_ok = True
            record.recruitment_test_due_soon = False
            record.recruitment_test_late = False
        
        elif record.recruitment_test_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.recruitment_test_ok = False
            record.recruitment_test_due_soon = True
            record.recruitment_test_late = False

        else:
            record.recruitment_test_ok = False
            record.recruitment_test_due_soon = False
            record.recruitment_test_late = True
            
            
    def _get_calcul_conv_te(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 23):
            record.conv_te = obj_rec.id
            
          
        if record.conv_te_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.conv_te_ok = True
            record.conv_te_due_soon = False
            record.conv_te_late = False
        
        elif record.conv_te_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.conv_te_ok = False
            record.conv_te_due_soon = True
            record.conv_te_late = False

        else:
            record.conv_te_ok = False
            record.conv_te_due_soon = False
            record.conv_te_late = True
            
            
    def _get_calcul_test_ok(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 21):
            record.test_ok = obj_rec.id
            
          
        if record.test_ok_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.test_ok_ok = True
            record.test_ok_due_soon = False
            record.test_ok_late = False
        
        elif record.test_ok_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.test_ok_ok = False
            record.test_ok_due_soon = True
            record.test_ok_late = False

        else:
            record.test_ok_ok = False
            record.test_ok_due_soon = False
            record.test_ok_late = True
            
    
    def _get_calcul_cas_injoignable(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 25):
            record.cas_injoignable = obj_rec.id
            
          
        if record.cas_injoignable_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.cas_injoignable_ok = True
            record.cas_injoignable_due_soon = False
            record.cas_injoignable_late = False
        
        elif record.cas_injoignable_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.cas_injoignable_ok = False
            record.cas_injoignable_due_soon = True
            record.cas_injoignable_late = False

        else:
            record.cas_injoignable_ok = False
            record.cas_injoignable_due_soon = False
            record.cas_injoignable_late = True
            
            
    def _get_calcul_entretien_rh(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 17):
            record.entretien_rh = obj_rec.id
            
          
        if record.entretien_rh_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.entretien_rh_ok = True
            record.entretien_rh_due_soon = False
            record.entretien_rh_late = False
        
        elif record.entretien_rh_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.entretien_rh_ok = False
            record.entretien_rh_due_soon = True
            record.entretien_rh_late = False

        else:
            record.entretien_rh_ok = False
            record.entretien_rh_due_soon = False
            record.entretien_rh_late = True
            
    def _get_calcul_conv_ent(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 24):
            record.conv_ent = obj_rec.id
            
          
        if record.conv_ent_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.conv_ent_ok = True
            record.conv_ent_due_soon = False
            record.conv_ent_late = False
        
        elif record.conv_ent_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.conv_ent_ok = False
            record.conv_ent_due_soon = True
            record.conv_ent_late = False

        else:
            record.conv_ent_ok = False
            record.conv_ent_due_soon = False
            record.conv_ent_late = True
            
            
    def _get_calcul_rh_ok(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 22):
            record.rh_ok = obj_rec.id
            
          
        if record.rh_ok_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.rh_ok_ok = True
            record.rh_ok_due_soon = False
            record.rh_ok_late = False
        
        elif record.rh_ok_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.rh_ok_ok = False
            record.rh_ok_due_soon = True
            record.rh_ok_late = False

        else:
            record.rh_ok_ok = False
            record.rh_ok_due_soon = False
            record.rh_ok_late = True
            
    def _get_calcul_morality_survey(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 13):
            record.morality_survey = obj_rec.id
            
          
        if record.morality_survey_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.morality_survey_ok = True
            record.morality_survey_due_soon = False
            record.morality_survey_late = False
        
        elif record.morality_survey_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.morality_survey_ok = False
            record.morality_survey_due_soon = True
            record.morality_survey_late = False

        else:
            record.morality_survey_ok = False
            record.morality_survey_due_soon = False
            record.morality_survey_late = True
            
    def _get_calcul_affecting_test(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 15):
            record.affecting_test = obj_rec.id
            
          
        if record.affecting_test_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.affecting_test_ok = True
            record.affecting_test_due_soon = False
            record.affecting_test_late = False
        
        elif record.affecting_test_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.affecting_test_ok = False
            record.affecting_test_due_soon = True
            record.affecting_test_late = False

        else:
            record.affecting_test_ok = False
            record.affecting_test_due_soon = False
            record.affecting_test_late = True
            
    def _get_calcul_entretien_n1(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 14):
            record.entretien_n1 = obj_rec.id
            
          
        if record.entretien_n1_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.entretien_n1_ok = True
            record.entretien_n1_due_soon = False
            record.entretien_n1_late = False
        
        elif record.entretien_n1_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.entretien_n1_ok = False
            record.entretien_n1_due_soon = True
            record.entretien_n1_late = False

        else:
            record.entretien_n1_ok = False
            record.entretien_n1_due_soon = False
            record.entretien_n1_late = True
            
    def _get_calcul_conv_ent_n1(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 26):
            record.conv_ent_n1 = obj_rec.id
            
          
        if record.conv_ent_n1_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.conv_ent_n1_ok = True
            record.conv_ent_n1_due_soon = False
            record.conv_ent_n1_late = False
        
        elif record.conv_ent_n1_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.conv_ent_n1_ok = False
            record.conv_ent_n1_due_soon = True
            record.conv_ent_n1_late = False

        else:
            record.conv_ent_n1_ok = False
            record.conv_ent_n1_due_soon = False
            record.conv_ent_n1_late = True

    
    def _get_calcul_successful_application(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 12):
            record.successful_application = obj_rec.id
            
          
        if record.successful_application_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.successful_application_ok = True
            record.successful_application_due_soon = False
            record.successful_application_late = False
        
        elif record.successful_application_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.successful_application_ok = False
            record.successful_application_due_soon = True
            record.successful_application_late = False

        else:
            record.successful_application_ok = False
            record.successful_application_due_soon = False
            record.successful_application_late = True  
            
    def _get_calcul_viver(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 3):
            record.viver = obj_rec.id
            
          
        if record.viver_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.viver_ok = True
            record.viver_due_soon = False
            record.viver_late = False
        
        elif record.viver_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.viver_ok = False
            record.viver_due_soon = True
            record.viver_late = False

        else:
            record.viver_ok = False
            record.viver_due_soon = False
            record.viver_late = True   
            
            
    def _get_calcul_unsuccessful_application(self, record, x, id_check):
        
        obj_rec  = self.env['hr.recruitment.stage'].search([('id','=',id_check)])
       
        
        if (record.stage_id.id == 2):
            record.unsuccessful_application = obj_rec.id
            
          
        if record.unsuccessful_application_durations < obj_rec.optimal_duration and record.diffdate_open_now < 32:
            record.unsuccessful_application_ok = True
            record.unsuccessful_application_due_soon = False
            record.unsuccessful_application_late = False
        
        elif record.unsuccessful_application_durations < obj_rec.duration_soon and record.diffdate_open_now < 39:
            record.unsuccessful_application_ok = False
            record.unsuccessful_application_due_soon = True
            record.unsuccessful_application_late = False

        else:
            record.unsuccessful_application_ok = False
            record.unsuccessful_application_due_soon = False
            record.unsuccessful_application_late = True  